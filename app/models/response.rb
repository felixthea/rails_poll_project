class Response < ActiveRecord::Base
  [:user_id, :answer_id].each do |field|
    attr_accessible field
    validates field, presence: true
  end

  validate :respondent_has_not_already_answered_question
  validate :respondent_is_author

  belongs_to(
    :answer_choice,
    class_name: "AnswerChoice",
    foreign_key: :answer_id,
    primary_key: :id
  )

  belongs_to(
    :respondent,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

  def respondent_is_author
    author_id = self
                .answer_choice
                .question
                .poll
                .author_id

    if author_id == user_id
      errors[:base] << "You cannot answer your own questions!!"
    end
  end

  def respondent_has_not_already_answered_question
    responses = existing_responses

    if responses.length == 1 && responses[0].id == self.id

    elsif !responses.empty?
      errors[:base] << "You already answered this question. No take-backs. You shill."
    end
  end

  private

  def existing_responses
    question_id = Response.find_by_sql(["SELECT answer_choices.question_id
    FROM answer_choices
    WHERE answer_choices.id = ?", answer_id]).first.question_id.to_i

    Response.find_by_sql(["SELECT *
            FROM responses
            JOIN answer_choices ON responses.answer_id = answer_choices.id
            WHERE answer_choices.question_id = ? AND responses.user_id = ?", question_id, user_id])
  end
end