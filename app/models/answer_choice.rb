class AnswerChoice < ActiveRecord::Base
  [:question_id, :text].each do |field|
    attr_accessible field
    validates field, presence: true
  end

  belongs_to(
    :question,
    class_name: "Question",
    foreign_key: :question_id,
    primary_key: :id
  )

  has_many(
    :responses,
    class_name: "Response",
    foreign_key: :answer_id,
    primary_key: :id
  )
end