class Poll < ActiveRecord::Base
  [:title, :author_id].each do |field|
    attr_accessible field
    validates field, presence: true
  end

  belongs_to(
    :author,
    class_name: "User",
    foreign_key: :author_id,
    primary_key: :id
  )

  has_many(
    :questions,
    class_name: "Question",
    foreign_key: :poll_id,
    primary_key: :id
  )

  def self.question_totals
    #finds total questions for each poll
    # polls_count | title
    self.find_by_sql(["SELECT COUNT(polls.id) AS polls_count, polls.title
    FROM questions
    JOIN polls ON polls.id = questions.poll_id
    GROUP BY polls.id
    "])
  end
end