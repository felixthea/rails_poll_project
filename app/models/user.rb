class User < ActiveRecord::Base
  [:user_name].each do |field|
    attr_accessible field
    validates field, :presence => true, :uniqueness => true
  end

  has_many(
    :authored_polls,
    class_name: "Poll",
    foreign_key: :author_id,
    primary_key: :id
  )

  has_many(
    :responses,
    class_name: "Response",
    foreign_key: :user_id,
    primary_key: :id
  )

  def self.questions
    #Users and the questions they have answered
    # user_name | qid | count_qs
    User.find_by_sql(["
    SELECT users.user_name, questions.id AS qid, COUNT(questions.id) AS count_qs
    FROM users
    JOIN responses ON users.id = responses.user_id
    JOIN answer_choices ON responses.answer_id = answer_choices.id
    JOIN questions ON questions.id = answer_choices.question_id
    GROUP BY questions.id, users.user_name
    "])
  end

  def self.question_count
    #Users and the questions they have answered
    # user_name | count | text
    self.find_by_sql(["SELECT COUNT(questions.id), users.user_name, questions.text
    FROM users
    JOIN responses ON users.id = responses.user_id
    JOIN answer_choices ON responses.answer_id = answer_choices.id
    JOIN questions ON questions.id = answer_choices.question_id
    GROUP BY questions.id, users.user_name
    "])
  end

  def self.answered_questions
    #Users and the questions they have answered
    # user_name | poll_id | count_qs
    self.find_by_sql(["
    SELECT questions.poll_id, users.user_name, COUNT(questions.id) AS count_qs
    FROM users
    JOIN responses ON users.id = responses.user_id
    JOIN answer_choices ON responses.answer_id = answer_choices.id
    JOIN questions ON questions.id = answer_choices.question_id
    GROUP BY questions.poll_id, users.user_name
    "])
  end

  def completed_polls
    active_polls.map do |poll|
      Poll.find_by_id(poll.id) if poll.completed == poll.total
    end
  end

  def completed_polls_other
    polls_taken = []

    user_responses = self.responses.includes(:answer_choice)

    user_responses.each do |response|
      poll_taken = response.answer_choice.question.poll

      polls_taken << poll_taken unless polls_taken.include?(poll_taken)
    end


    poll_count = Hash.new(0)

    user_responses.each do |user_response|
      # calculate how many questions were answered for the current poll
      poll_count[user_response.answer_choice.question.poll_id] += 1
    end

    completed_polls = []
    poll_count.each do |poll_id, num_answered|
      new_poll = Poll.find_by_id(poll_id)
      completed_polls << new_poll if num_answered == new_poll.questions.length
    end

    completed_polls
  end

  def uncompleted_polls
    active_polls.map do |poll|
      Poll.find_by_id(poll.id) if poll.completed < poll.total
    end
  end


  private

  def active_polls
    query = <<-SQL
       INNER JOIN (SELECT polls.id AS p_id, COUNT(polls.id) AS polls_count
       FROM questions
       JOIN polls ON polls.id = questions.poll_id
       GROUP BY polls.id) AS t_p ON questions.poll_id = t_p.p_id
     SQL

     self
     .responses
     .joins(answer_choice: {question: :poll})
     .joins(query)
     .select("questions.poll_id AS poll_id, COUNT(questions.id) AS completed, t_p.polls_count AS total")
     .group("questions.poll_id, t_p.polls_count")
   end

  def active_polls_by_sql
    # user | poll_id | count_qs | polls_count
    sql = <<-SQL
        (SELECT questions.poll_id AS id, users.user_name,
                COUNT(questions.id) AS completed,
                t_p.polls_count AS total
        FROM users
        JOIN responses ON users.id = responses.user_id
        JOIN answer_choices ON responses.answer_id = answer_choices.id
        JOIN questions ON questions.id = answer_choices.question_id
        JOIN (SELECT polls.id AS p_id, COUNT(polls.id) AS polls_count
              FROM questions
              JOIN polls ON polls.id = questions.poll_id
              GROUP BY polls.id) AS t_p ON questions.poll_id = t_p.p_id
        WHERE users.id = ?
        GROUP BY questions.poll_id, users.user_name,t_p.polls_count)
        SQL

    Poll.find_by_sql([sql, self.id])
  end
end