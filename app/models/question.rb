class Question < ActiveRecord::Base
  [:poll_id, :text].each do |field|
    attr_accessible field
    validates field, presence: true
  end

  has_many(
    :answer_choices,
    class_name: "AnswerChoice",
    foreign_key: :question_id,
    primary_key: :id
  )

  belongs_to(
    :poll,
    class_name: "Poll",
    foreign_key: :poll_id,
    primary_key: :id
  )

  def self.polls
    #questions with their polls
    # id | question | answer
    self.find_by_sql(["
    SELECT questions.id, questions.text AS question, answer_choices.id, answer_choices.text AS answer
    FROM questions
    JOIN answer_choices ON questions.id = answer_choices.question_id
    ORDER BY questions.id, questions.text
    "])
  end

  def results
    answer_choices = self.answer_choices.includes(:responses)

    results = {}
    answer_choices.each do |choice|
      results[choice.text] = choice.responses.length
    end

    results
  end
end

#poll.questions.select("answer_choices.*").joins(:answer_choices)