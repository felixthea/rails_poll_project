# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!([{ user_name: 'Adam' }, { user_name: 'Felix'}, { user_name: 'Bob' }])
Poll.create!([{ title: 'Favorite Foods', author_id: 1 },
              { title: 'Favorite Movies', author_id: 2 }])

Question.create!([{ poll_id: 1, text: 'Do you like pizza?' },
                 { poll_id: 1, text: 'Your favorite pizza toppings'}
                 ])
Question.create!([{ poll_id: 2, text: 'Favorite sci-fi movie?'},
                  poll_id: 2, text: 'Favorite comedy movie?'])

Question.create!([ { poll_id: 1, text: 'Favorite genre.'},
                   { poll_id: 1, text: 'Your favorite pizza place'},
                   { poll_id: 1, text: 'Best pizza story...'}])

AnswerChoice.create!([{ question_id: 1, text: 'Yes'},
                      { question_id: 1, text: 'No'}])
AnswerChoice.create!([{ question_id: 2, text: 'Pepperoni'},
                      { question_id: 2, text: 'Mushroom'},
                      { question_id: 2, text: 'Hawaiian' }])
AnswerChoice.create!([{question_id: 3, text: "Gattaca"},
                      {question_id: 3, text: "Fifth Element"},
                      {question_id: 3, text: "Blade Runner"},
                      {question_id: 3, text: "Total Recall"},
                      {question_id: 3, text: "District 9"}
                      ])
AnswerChoice.create!([{question_id: 4, text: "Something about Mary"},
                      {question_id: 4, text: "The Hangover"},
                      {question_id: 4, text: "Bridesmaids"},
                      {question_id: 4, text: "Tommyboy"},
                      {question_id: 4, text: "Blazing Saddles"}
                      ])

AnswerChoice.create!({ question_id: 6, text: "Romanos"  })
AnswerChoice.create!({ question_id: 6, text: "Olympic Pizza"  })
AnswerChoice.create!({ question_id: 6, text: "Mystic Pizza"  })
AnswerChoice.create!({ question_id: 6, text: "Pyzz"  })

AnswerChoice.create!({ question_id: 7, text: "I was going to this pizza place..."  })
AnswerChoice.create!({ question_id: 7, text: "I met my proposed at one."  })
AnswerChoice.create!({ question_id: 7, text: "I worked at one in High School."  })
AnswerChoice.create!({ question_id: 5, text: "Fantasy"  })
AnswerChoice.create!({ question_id: 5, text: "Sci-Fi"  })
AnswerChoice.create!({ question_id: 5, text: "Drama"  })
AnswerChoice.create!({ question_id: 5, text: "Romance"  })
AnswerChoice.create!({ question_id: 5, text: "Comedy"  })
AnswerChoice.create!({ question_id: 5, text: "Mystery"  })
AnswerChoice.create!({ question_id: 5, text: "Horror"  })

Response.create!({ user_id: 2, answer_id: 24 })
Response.create!({ user_id: 2, answer_id: 22 })
Response.create!({ user_id: 2, answer_id: 16 })
Response.create!({ user_id: 1, answer_id: 17 })

Question.create!({ poll_id: 2, text: 'Favorite actor.'})
AnswerChoice.create!({ question_id: 8, text: "Bruce Willis"  })
AnswerChoice.create!({ question_id: 8, text: "Daniel Craig"  })
AnswerChoice.create!({ question_id: 8, text: "Jeff Bridges"  })

Question.create!({ poll_id: 2, text: 'Favorite actress.'})
AnswerChoice.create!({ question_id: 9, text: "Jodie Foster"  })
AnswerChoice.create!({ question_id: 9, text: "Meryl Streep"  })
AnswerChoice.create!({ question_id: 9, text: "Raquel Welch"  })

Response.create!({ user_id: 3, answer_id: 30})
Response.create!({ user_id: 3, answer_id: 24})