class CreateAnswerChoicesAndResponses < ActiveRecord::Migration
  def change
    create_table :answer_choices do |t|
      t.integer :question_id
      t.string :text

      t.timestamps
    end

    create_table :responses do |t|
      t.integer :user_id
      t.integer :answer_id

      t.timestamps
    end
  end
end
